import re


def same(item, target_word):  # checks if words have letters matching
    return len([i for (i, t) in zip(item, target_word) if i == t])


def build(pattern, available_words, seen, built_words):  # generates a word based on a set pattern and matches to word
    return [word for word in available_words
            if re.search(pattern, word) and word not in seen.keys() and  # re.search finds words that matches the
            word not in built_words]  # current word and is not already in matched or not seen in the path


def find(start_word, available_words, seen, target_word, word_path, short, excluded):
    match_words = []
    rare = ['j', 'q', 'x', 'z']
    for i in range(len(start_word)):  # generates word match_words
        match_words += build(start_word[:i] + "." + start_word[i + 1:], available_words, seen, match_words)
    for w in match_words:  # Removes words if in word list or contain rare letters
        if w in excluded:  # removes word words
            match_words.remove(w)
        if short:
            for l in rare:  # removes words with rare letters if letter not in target_word word
                if l not in target_word:
                    if l in w:
                        match_words.remove(w)
    if len(match_words) == 0:
        return False  # exits code when there is no path
    match_words = sorted([(same(w, target_word), w) for w in match_words])
    if short:
        match_words.reverse()
    for (num_match, match_wrd) in match_words:  # compares word with target_word for matching letters
        if num_match >= len(target_word) - 1:
            if num_match == len(target_word) - 1:
                word_path.append(match_wrd)
            return True  # where code exits upon getting to the last word
        seen[match_wrd] = True
    for (match, match_wrd) in match_words:
        word_path.append(match_wrd)  # adds new item to path match_words
        if find(match_wrd, available_words, seen, target_word, word_path, short, excluded):
            return True
        word_path.pop()


def check_word(text, dictionary):  # Checks if input is text
    while True:
        try:
            output = input(text)
            output = output.lower()  # converts input into lower case
            output = output.replace(' ', '')  # removes any white space from input
            if output.isalpha():  # checks if input is all letters
                if output in dictionary:
                    return output
                else:
                    print('Please enter a valid word.')
            else:
                print('Please enter a valid word.')
        except:
            print('Please enter a valid word.')
            continue
