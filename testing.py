import unittest
import Functions

words = ['hide', 'seek', 'lead', 'gold', 'side', 'goad', 'goal', 'load', 'site']
word = 'hide'
seen = {'lead': True, 'hide': True}
seen2 = {'lead': True, 'load': True, 'goad': True}
path = []
path2 = ['lead', 'load', 'goad']
exclude = []


class AssignmentTest(unittest.TestCase):
    def test_same(self):
        self.assertEqual(Functions.same('3', 'seek'), 0)
        self.assertEqual(Functions.same('lead', 'gold'), 1)
        self.assertEqual(Functions.same('heed', 'seek'), 2)
        self.assertEqual(Functions.same('goad', 'gold'), 3)
        self.assertEqual(Functions.same('hide', 'hide'), 4)

    def test_build(self):
        self.assertEqual(Functions.build('.ide', words, seen, [])[0], 'side')
        self.assertEqual(Functions.build('l.ad', words, seen, [])[0], 'load')
        self.assertEqual(Functions.build('go.d', words, seen, [])[0], 'gold')
        self.assertEqual(Functions.build('goa.', words, seen, [])[0], 'goad')

    def test_find(self):
        self.assertTrue(Functions.find('lead', words, seen, 'gold', path, True, exclude))
        self.assertTrue(Functions.find('goad', words, seen, 'gold', path2, True, exclude))
        self.assertFalse(Functions.find('bomb', words, seen, 'gold', path, True, exclude))
        self.assertFalse(Functions.find('size', words, seen, 'seek', path, True, exclude))

if __name__ == '__main__':
    unittest.main()