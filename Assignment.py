import Functions


while True:  # Checks and opens dictionary
    try:
        FileName = input('Enter dictionary name: ')
        FileName = FileName.strip()
        file = open(FileName)
        lines = file.readlines()
        break
    except:
        print('Please enter Valid Dictionary.')

dictionary = []  # creates list to check for valid words from input

for line in lines:
    word = line.rstrip()
    dictionary.append(word)

while True:
    start = Functions.check_word('Enter start word: ', dictionary)  # runs check_input function

    while True:  # checks if start length match target_word length
        target = Functions.check_word('Enter target word: ', dictionary)
        if len(start) == len(target):  # Compares word length
            break
        else:
            print('Please enter a word that is', len(start), 'letters long.')
    while True:
        try:
            short_long = input('Do you want the shortest (s) or longest (l) path? ')
            short_long = short_long.lower()
            short_long = short_long.replace(' ', '')
            if short_long.isalpha():
                if short_long == 's' or short_long == 'shortest':
                    shortest = True
                    print('You have chosen to take the shortest path.')
                    break
                elif short_long == 'l' or short_long == 'longest':
                    shortest = False
                    print('You have chosen to take the longest path. This may take more time than the shortest path.')
                    break
                else:
                    print("Please enter 's' for shortest or 'l' for longest path.")
            else:
                print("Please enter 's' for shortest or 'l' for longest path.")
        except:
            print("Please enter 's' for shortest or 'l' for longest path.")
    while True:
        try:
            error = 0
            exclude = []  # creates list for word words
            word = input("Enter words to be excluded separated by ',' or press 'enter' to skip: ")  # prompts user to input
            if len(word) < 1:
                print('No words are excluded.')
                break
            word = word.lower()
            word = word.replace(' ', '')
            word = word.strip().split(',')
            for wrd in word:
                if wrd == '':
                    continue
                elif wrd in dictionary:
                    exclude.append(wrd)
                elif wrd not in dictionary:
                    error += 1
                    print('Please check the word', wrd, 'is a valid word.')
            if error == 0:
                print('Excluded words are:', ', '.join(exclude))
                break
        except:
            print('Enter a valid word.')
            continue

    usable_words = []

    for line in lines:  # creates list with words matching input word length
        word = line.rstrip()
        if len(word) == len(start):  # selects words with same length
            usable_words.append(word)

    path = [start]  # sets up list
    seen = {start: True}

    if Functions.find(start, usable_words, seen, target, path, shortest, exclude):  # runs main function
        path.append(target)  # adds target word to the end of word_path
        print(len(path) - 1, ' -> '.join(path))  # .join(word_path) joins list together for better print format
    else:  # displays when no word_path is found
        print("No word_path found.")

    while True:
        repeat = input('Do you want to start again? y/n ')  # allows user to continue without exiting
        repeat = repeat.lower()
        if repeat == 'y':
            break
        elif repeat == 'n':
            exit()
        else:
            print("Please enter 'y' or 'n'.")
